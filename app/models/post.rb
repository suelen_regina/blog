class Post < ActiveRecord::Base
	acts_as_votable
	belongs_to :user
	has_attached_file :photo, styles: { medium: "600x400>", thumb: "150x100>" }, default_url: "/images/:style/missing.png"
  	validates_attachment_content_type :photo, content_type: /\Aimage\/.*\Z/
  	validates :photo, attachment_presence: true
	validates_with AttachmentSizeValidator, attributes: :photo, less_than: 5.megabytes
	has_many :comments, dependent: :destroy
	validates :title,
		uniqueness: true,
		presence: true
	validates :body,
		length: {minimum: 100, maximum:5000}
	validates :user,
		presence: true
end
